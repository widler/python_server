"""
Серверное приложение для соединений
"""
import asyncio
from asyncio import transports


class ClientProtocol(asyncio.Protocol):
    login: str
    server: 'Server'
    transport: transports.Transport

    def __init__(self, server: 'Server'):
        self.server = server
        self.login = None

    def send_history(self):
        for msg in self.server.history:
            self.transport.write(
                msg.encode()
            )

    def is_user_exists(self, name):
        # Вариантов решения несколько можно создать
        for login in self.server.clients:
            if login == name:
                return True
        return False

    def data_received(self, data: bytes):
        decoded = data.decode()
        print(decoded)

        if self.login is None:
            # login:User
            if decoded.startswith("login:"):
                # не уверен что правильно понял задание, но делать отключение скучно
                # я решил сделать что бы система сообщала что имя занято, предлагала новый вариант логина
                # и позволяла пользоватлю возможность залогиниться снова

                login = decoded.replace("login:", "").replace("\r\n", "")
                if not self.is_user_exists(self, login):
                    self.login = login
                    self.send_history(self)
                    self.transport.write(
                        f"Привет, {self.login}!".encode()
                    )
                else:
                    i = 1
                    while self.is_user_exists(self, f"{login}{i}"):
                        i += 1
                    self.transport.write(
                        f"Извините, имя {self.login} занято. Можем прдложить {login}{i}"
                    )
        else:
            self.send_message(decoded)

    def send_message(self, message):
        format_string = f"<{self.login}> {message}"
        if len(self.server.history) >=10:
            self.server.history.pop(0)
        self.server.history.append(format_string)
        encoded = format_string.encode()
        for client in self.server.clients:
            if client.login != self.login:
                client.transport.write(encoded)

    def connection_made(self, transport: transports.Transport):
        self.transport = transport
        self.server.clients.append(self)
        print("Соединение установлено")

    def connection_lost(self, exception):
        self.server.clients.remove(self)
        print("Соединение разорвано")


class Server:
    clients: list
    history: list

    def __init__(self):
        self.clients = []
        self.history = []

    def create_protocol(self):

        return ClientProtocol(self)

    async def start(self):
        loop = asyncio.get_running_loop()

        coroutine = await loop.create_server(
            self.create_protocol,
            "127.0.0.1",
            8888
        )

        print("Сервер запущен ...")

        await coroutine.serve_forever()


process = Server()
try:
    asyncio.run(process.start())
except KeyboardInterrupt:
    print("Сервер остановлен вручную")
